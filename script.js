/*
Практичне завдання.
Реалізувати функцію підсвічування клавіш.

Технічні вимоги:
- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір.
 При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною.
 Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір.
 Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
*/

const buttonList = document.querySelectorAll('.btn');
const buttonArr = [...buttonList];

window.addEventListener('keydown', onKeyPressChangeColor);

function onKeyPressChangeColor(e) {
    buttonArr.forEach((value) => {
        if (value.textContent.toLowerCase() === e.key.toLowerCase()) {
            value.style.backgroundColor = 'blue';
        } else {
            value.style.backgroundColor = 'black';
        }
    })
}


